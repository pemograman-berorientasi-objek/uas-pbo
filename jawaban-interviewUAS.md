# 1.  Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:
### Tabel Users
| No | Use Case | Score |
| --- | --- | --- |
| 1 | User dapat mendaftar akun baru dengan username dan password | 100 |
| 2 | User dapat masuk ke akun OneDrive menggunakan username dan password | 100 |
| 3 | User dapat mengubah username dan password akun | 90 |
| 4 | User dapat menghapus akun OneDrive | 80 |
| 5 | User dapat melihat informasi profil akun | 90 |
| 6 | User dapat mengubah informasi profil akun seperti nama, alamat email, dan foto profil | 90 |


# 2. Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital

![Dokumentasi](Dokumentasi/ClassDiagrm.png)

# 3. Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle

- Single Responsibility Principle (SRP):

Pemisahan fungsi-fungsi yang berbeda menjadi kelas-kelas terpisah untuk melakukan tugas-tugas yang berbeda. Misalnya, pemisahan kode untuk mengelola file dan kode untuk mengelola folder ke dalam kelas-kelas terpisah.

```
 <?php
                // Menghubungkan ke database
                $servername = "localhost";
                $username = "root";
                $password = "";
                $dbname = "db_onedrive";

                $conn = new mysqli($servername, $username, $password, $dbname);
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }

                // Mengambil data folder dari tabel folders
                $sql = "SELECT * FROM folders";
                $result = $conn->query($sql);

                // Menampilkan daftar folder dalam dropdown
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        echo '<option value="'.$row["folders_name"].'">'.$row["folders_name"].'</option>';
                    }
                } else {
                    echo '<option value="" disabled>No folders found.</option>';
                }

                // Menutup koneksi
                $conn->close();
                ?>
            </select>
            <input type="submit" value="Upload File" name="submit" class="btn btn-primary">
        </form>
        <h2 class="mt-4">Daftar Folder:</h2>
        <div class="list-group">
            <?php
                // Menghubungkan ke database
                $servername = "localhost";
                $username = "root";
                $password = "";
                $dbname = "db_onedrive";

                $conn = new mysqli($servername, $username, $password, $dbname);
                if ($conn->connect_error) {
                    die("Connection failed: " . $conn->connect_error);
                }

                // Mengambil data folder dari tabel folders
                $sql = "SELECT * FROM folders";
                $result = $conn->query($sql);

                // Menampilkan daftar folder
                if ($result->num_rows > 0) {
                    while ($row = $result->fetch_assoc()) {
                        echo '<a ="uploads/'.$row["folders_name"].'" class="list-group-item list-group-item-action">
                                <i class="fas fa-folder"></i> '.$row["folders_name"].'
                              </a>';
                    }
                } else {
                    echo '<p>No folders found.</p>';
                }

                // Menutup koneksi
                $conn->close();
            ?>
```
- Open/Closed Principle (OCP):

Menggunakan pola desain seperti Polimorfisme dan Pewarisan untuk memungkinkan ekstensibilitas dan pengubahan kode tanpa mengubah kode yang sudah ada.


```
<?php
// Mendapatkan daftar file dari direktori
$files = scandir("uploads/");

$totalSize = 0; // Variabel untuk menyimpan total ukuran file

// Menampilkan daftar file
foreach ($files as $file) {
    if ($file != "." && $file != "..") {
        $filePath = "uploads/" . $file;
        $fileSize = filesize($filePath); // Menghitung ukuran file

        echo '<div class="list-group-item d-flex justify-content-between align-items-center">
                <a href="uploads/'.$file.'" class="me-2">
                    <i class="fas fa-file"></i> '.$file.'
                </a>
                <span class="badge bg-primary rounded-pill">'.$fileSize.' bytes</span>
                <a href="delete_file.php?filename='.$file.'" class="text-danger" onclick="return confirm(\'Apakah Anda yakin ingin menghapus file ini?\')">
                    <i class="fas fa-trash"></i>
                </a>
            </div>';

        $totalSize += $fileSize; // Menambahkan ukuran file ke total ukuran
    }
}

// Konversi total ukuran ke MB
$totalSizeMB = round($totalSize / (1024 * 1024), 2);

echo '<div class="mt-3">Total Size: '.$totalSizeMB.' MB</div>';
?>
```
- Liskov Substitution Principle (LSP):
Menjamin bahwa objek-objek turunan dapat digunakan sebagai pengganti objek induknya tanpa mengganggu fungsionalitas yang diharapkan.

- Interface Segregation Principle (ISP):

Memisahkan antarmuka yang besar dan kompleks menjadi antarmuka yang lebih kecil dan lebih khusus untuk setiap kelas atau komponen.

- Dependency Inversion Principle (DIP):

Menggunakan pola Dependency Injection atau Inversion of Control (IoC) container untuk mengurangi ketergantungan langsung antara kelas-kelas.

# 4. Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

- Model-View-Controller (MVC):

Kode tersebut mungkin dapat diorganisir berdasarkan pola desain Model-View-Controller (MVC). Misalnya, file-file PHP terkait dengan tampilan (view) dapat dipisahkan dari logika bisnis (controller) dan pemodelan data (model).
Dependency Injection (DI):

= Pola desain Dependency Injection dapat diterapkan untuk mengurangi ketergantungan langsung antara kelas-kelas dan meningkatkan pengujian dan fleksibilitas kode. Misalnya, objek koneksi database dapat disuntikkan ke kelas-kelas yang membutuhkannya.
Factory Method:

Jika ada beberapa jenis objek yang perlu dibuat, pola desain Factory Method dapat digunakan untuk memisahkan proses pembuatan objek dan membuatnya lebih fleksibel. Misalnya, jika terdapat jenis folder yang berbeda dengan metode pembuatan yang berbeda, pola Factory Method dapat digunakan untuk membuat objek folder yang sesuai.
Observer:

Jika ada komponen-komponen dalam sistem yang perlu mengamati dan bereaksi terhadap perubahan pada objek lainnya, pola desain Observer dapat diterapkan. Misalnya, jika ada mekanisme untuk memberi tahu pengguna ketika ada perubahan pada folder atau file, pola Observer dapat digunakan.


# 5. Mampu menunjukkan dan menjelaskan konektivitas ke database
![Dokumentasi](Dokumentasi/koneksi.png)

$host = "localhost";: Mendefinisikan variabel $host yang berisi nama host database, dalam hal ini "localhost".

$username = "root";: Mendefinisikan variabel $username yang berisi username untuk mengakses database, dalam hal ini "root".

$password = "";: Mendefinisikan variabel $password yang berisi password untuk mengakses database, dalam hal ini kosong (tanpa password).

$db = "db_onedrive";: Mendefinisikan variabel $db yang berisi nama database yang akan digunakan, dalam hal ini "db_onedrive".

$conn = mysqli_connect($host, $username, $password, $db) or die (mysqli_error());: Melakukan koneksi ke database MySQL menggunakan fungsi mysqli_connect(). Fungsi ini menerima empat parameter yaitu hostname, username, password, dan nama database. Jika koneksi berhasil, variabel $conn akan berisi objek koneksi. Jika koneksi gagal, akan muncul pesan error menggunakan fungsi mysqli_error() dan skrip akan berhenti di situ.

Dengan menggunakan kode di atas, koneksi ke database MySQL akan dibuat menggunakan nilai host, username, password, dan nama database yang telah ditentukan. Koneksi ini nantinya dapat digunakan untuk melakukan query dan interaksi lainnya dengan database menggunakan fungsi-fungsi mysqli yang disediakan.

# 6. Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya

- Web service yang dipakai

Slim FrameWork V3

- web page untuk user
![Dokumentasi](Dokumentasi/home.png)


- Web Service Untuk Aplikasi
![Dokumentasi](Dokumentasi/webservice.png)

- Tampilan CRUD

![Dokumentasi](Dokumentasi/creat.png)
Tampilan diatas berfungsi sebagai membuat file baru yang diambil di direktori device user

![Dokumentasi](Dokumentasi/creat.png)
Tampilan diatas berfungsi sebagai informasi dari web onedrive

![Dokumentasi](Dokumentasi/update.png)
Tampilan diatas berfungsi sebagai mengedit nama folder jika user merasa nama folder kurang cocok atau salah

![Dokumentasi](Dokumentasi/delete.png)
Tampilan diatas berfungsi sebagai mengahapus file

# 7. Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital


Berikut adalah penjelasan komponen GUI yang terdapat dalam kode tersebut:

1. Navbar:

Menampilkan navigasi situs dengan judul "OneDrive" sebagai brand dan menu navigasi yang mencakup pilihan seperti "Home", "Buat File", "Buat Folder", "Edit Folder", "Hapus Folder", dan "Logout".
Form Upload File:

Menampilkan sebuah form yang memungkinkan pengguna untuk mengunggah file. Terdapat input file dan dropdown untuk memilih folder tujuan. Tombol "Upload File" digunakan untuk mengirimkan form.

2. Daftar Folder:

Menampilkan daftar folder yang diperoleh dari database. Setiap folder ditampilkan sebagai item dalam komponen "list-group" dengan ikon folder dan nama folder.

3. Daftar File:

Menampilkan daftar file yang ada dalam folder "uploads". Setiap file ditampilkan sebagai item dalam komponen "list-group" dengan ikon file, nama file, dan ukuran file dalam bytes. Terdapat juga tombol "Delete" untuk menghapus file, yang mengarahkan ke halaman "delete_file.php".

4. Footer:

Menampilkan bagian footer dengan informasi tentang pencipta aplikasi dan hak cipta.
Dalam hal tampilan, kode tersebut menggunakan library Bootstrap CSS untuk mendapatkan tampilan yang responsif dan menarik. Font Awesome CSS digunakan untuk mendapatkan ikon-ikon yang digunakan dalam tampilan.

Secara keseluruhan, GUI dari produk digital yang menyerupai OneDrive ini memungkinkan pengguna untuk mengunggah file, melihat daftar folder, melihat daftar file dalam folder, dan menghapus file. Pengguna juga dapat melakukan navigasi menggunakan menu navigasi yang tersedia.

# 8. Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital

1. Permintaan (Request) GET:

Ketika pengguna membuka halaman pertama kali atau mengklik link menu navigasi seperti "Home", "Buat File", "Buat Folder", "Edit Folder", dan "Hapus Folder", browser akan mengirimkan permintaan GET ke server untuk mendapatkan halaman terkait. Contohnya adalah permintaan untuk file home.php, index.php, buat_folder.php, edit_folder.php, dan hapus_folder.php. Server akan merespons dengan mengirimkan halaman terkait kembali ke browser.

2. Permintaan (Request) POST:

Ketika pengguna mengisi form upload file dan mengklik tombol "Upload File", browser akan mengirimkan permintaan POST ke server dengan data form yang diisi. Permintaan ini ditujukan ke file upload.php. Server akan menerima permintaan tersebut, memproses data form, dan menyimpan file yang diunggah ke folder yang ditentukan.

3. Permintaan (Request) DELETE:

Ketika pengguna mengklik tombol "Delete" pada daftar file, browser akan mengirimkan permintaan DELETE ke server dengan parameter berupa nama file yang akan dihapus. Permintaan ini ditujukan ke file delete_file.php. Server akan menerima permintaan tersebut, mencari file yang sesuai, dan menghapusnya dari folder "uploads".

4. Konfirmasi (Confirmation) melalui fungsi JavaScript:

Ketika pengguna mengklik tombol "Logout" atau tombol "Delete" pada daftar file, muncul konfirmasi melalui fungsi JavaScript confirm() untuk memastikan apakah pengguna yakin ingin melanjutkan aksi tersebut. Jika pengguna menekan tombol "OK" pada konfirmasi, aksi tersebut akan dilanjutkan. Jika pengguna menekan tombol "Cancel", aksi tersebut akan dibatalkan.

5. Respon (Response) dari server:

Setiap permintaan yang dikirimkan oleh browser akan direspon oleh server. Server akan menghasilkan respon yang sesuai tergantung dari jenis permintaan yang diterima. Misalnya, halaman HTML yang dihasilkan oleh server akan dikirimkan kembali ke browser sebagai respon dari permintaan GET. Respon tersebut akan ditampilkan oleh browser dalam bentuk tampilan halaman web.


# 9. Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

https://youtu.be/Ul6vKvLbGxo
